# table creator

Class to generate table view for specified data array

## Usage

```
$data = [
    ['key1' => 'Value 1.1', 'key2' => 'Value 2.1'],
    ['key3' => 'Value 3.2'],
    ['key1' => 'Value 1.3', 'key2' => 'Value 2.3', 'key3' => 'Value 3.3']
];

echo (new TableGenerator($data, 'table1-id'))->getView();

$headerAliases = [
    'key1' => 'No',
    'key_undefined' => 'Wount show',
    'key2' => 'title',
    'key3' => 'description'
];

echo (new TableGenerator($data, 'table2-id'))->setHeaderAliases($headerAliases)->getView();
```